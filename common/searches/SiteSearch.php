<?php
namespace common\searches;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Book;


class SiteSearch extends Book
{
    /** @var string Фамилия и Имя автора */
    public $authorName;

    /** @var string ПК жанра */
    public $genre_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'genre_id'], 'integer'],
            [['authorName'], 'string'],
            [['name', 'public_at', 'genre'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'authorName' => 'Автор',
                'genre_id'   => 'Жанр'
            ]
        );
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SiteSearch::find()->alias('b');

        $query->joinWith('authors a');
        $query->joinWith('genres g');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

       $query->andFilterWhere(['b.id' => $this->id])
            ->andFilterWhere(['b.public_at' => $this->public_at])
            ->andFilterWhere(['LIKE', 'LOWER(b.name)', '%'.mb_strtolower($this->name).'%', false]);

        if (!empty($this->authorName)) {
            $query->andFilterWhere(['or',
                ['LIKE', 'LOWER(CONCAT(a.last_name, \' \', a.first_name))', '%'.mb_strtolower($this->authorName).'%', false],
                ['LIKE', 'LOWER(CONCAT(a.first_name, \' \', a.last_name))', '%'.mb_strtolower($this->authorName).'%', false],
            ]);
        }

        if (!empty($this->genre_id)) {
            $query->andFilterWhere(['g.id' => $this->genre_id]);
        }

        return $dataProvider;
    }
}
