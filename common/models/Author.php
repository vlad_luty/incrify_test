<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

/**
 * This is the model class for table "author".
 *
 * @property int $id
 * @property string $first_name Имя автора
 * @property string $last_name Фамилия автора
 * @property int $birth_day Дата рождения
 * @property string|null $phone Номер телефона
 *
 * @property AuthorBook[] $authorBooks
 * @property Book[] $books
 */
class Author extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%author}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'birth_day'], 'required'],
            [['first_name', 'last_name'], 'string', 'max' => 80],
            [['first_name', 'last_name'], 'match', 'pattern' => '/^[А-Яа-яёA-Za-z0-9]+$/u', 'message' => 'Разрешены только русские или латинские буквы'],
            [['birth_day'], 'validateBirthDay'],
            [['phone'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'Имя автора',
            'last_name' => 'Фамилия автора',
            'birth_day' => 'Дата рождения',
            'phone' => 'Номер телефона',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function beforeSave($insert)
    {
        $this->first_name = mb_strtolower($this->first_name);
        $this->last_name = mb_strtolower($this->last_name);
        $this->birth_day = strtotime($this->birth_day);
        return parent::beforeSave($insert);
    }

    /**
     * {@inheritDoc}
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->first_name = StringHelper::mb_ucfirst($this->first_name);
        $this->last_name  = StringHelper::mb_ucfirst($this->last_name);
        $this->birth_day  = date('Y-m-d', $this->birth_day);
    }

    // =======================
    // relations
    // =======================

    /**
     * Gets query for [[AuthorBooks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthorBooks()
    {
        return $this->hasMany(AuthorBook::className(), ['author_id' => 'id']);
    }

    /**
     * Gets query for [[Books]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Book::className(), ['id' => 'book_id'])->viaTable('author_book', ['author_id' => 'id']);
    }

    // =======================
    // validates
    // =======================

    /**
     * Валидация дня рождения на акутальность и верность наступления его
     */
    public function validateBirthDay()
    {
        if (strtotime($this->birth_day) >= time()) {
            $this->addError('birth_day', 'Дата рождения еще не наступила!');
        }
    }

    // =======================
    // getters
    // =======================

    /**
     * Формирование полного имени автора в зависимости от его имени
     * @return string Полное имя автора
     */
    public function getFullName() : string
    {
        $fullName = $this->last_name . ' ' . $this->first_name;

        if (preg_match('/^[a-z\s]+$/i', $fullName)) {
            return  $this->first_name . ' ' . $this->last_name;
        }
        return $fullName;
    }


    public static function getDropDown () {
        return ArrayHelper::map(static::find()->all(), 'id', function (Author $model) {
            return $model->getFullName();
        });
    }
}
