<?php

namespace common\models;

use common\helpers\DateHelper;
use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "book".
 *
 * @property int $id
 * @property string $name Название книги
 * @property string $description Описание
 * @property string|null $image Обложка
 * @property int $public_at Дата публикации
 *
 * @property AuthorBook[] $authorBooks
 * @property Author[] $authors
 * @property BookGenre[] $bookGenres
 * @property Genre[] $genres
 */
class Book extends \yii\db\ActiveRecord
{
    /**
     * @var UploadedFile Файл изображения
     */
    public $imageFile;

    /** @var Author[] */
    public $_authors = [];

    /** @var Genre[] */
    public $_genres = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%book}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'public_at'], 'required'],
            [['description'], 'string'],
            ['name', 'match', 'pattern' => '/^[а-яА-Яёa-zA-Z]+/', 'message' => 'Разрешены только русские или латинские буквы'],
            [['public_at'], 'validatePublicAt'],
            [['name', 'image'], 'string', 'max' => 250],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif, jpeg, webp'],
            [['imageFile'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'name'        => 'Название книги',
            'description' => 'Описание',
            'image'       => 'Обложка',
            'imageFile'   => 'Файл обложки',
            'public_at'   => 'Дата публикации',
        ];
    }

    public function beforeSave($insert)
    {
        $this->public_at = strtotime($this->public_at);
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (!empty($this->_authors)) {
            foreach ($this->_authors as $author) {
                $this->link('authors', $author);
            }
        }

        if (!empty($this->_genres)) {
            foreach ($this->_genres as $genre) {
                $this->link('genres', $genre);
            }
        }
    }


    /**
     * {@inheritdoc}
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->public_at = date('Y-m-d', $this->public_at);
    }

    public function load($data, $formName = null)
    {
        $formName = $formName ?: $this->formName();
        $load = parent::load($data, $formName);

        if (!empty($data[$formName]['authors'])) {
            foreach ($data[$formName]['authors'] as $author_id) {
                if ($author_id != 0) {
                    $this->_authors[] = Author::findOne(['id' => $author_id]);
                }
            }
        }
        if (!empty($data[$formName]['genres'])) {
            foreach ($data[$formName]['genres'] as $genre_id) {
                if ($genre_id != 0) {
                    $this->_genres[] = Genre::findOne(['id' => $genre_id]);
                }
            }
        }
        $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
        return $load;
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        $valid = parent::validate($attributeNames, $clearErrors);

        if ($valid && !empty($this->imageFile)) {
            $this->image = '/images/' . \md5($this->imageFile->baseName) . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs( '@frontend/web' . $this->image);
            return true;
        }
        return $valid;
    }


    //============================
    // relations
    //===========================

    /**
     * Gets query for [[AuthorBooks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthorBooks()
    {
        return $this->hasMany(AuthorBook::className(), ['book_id' => 'id']);
    }

    /**
     * Gets query for [[Authors]].
     *
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getAuthors()
    {
        return $this->hasMany(Author::className(), ['id' => 'author_id'])->viaTable('author_book', ['book_id' => 'id']);
    }

    /**
     * Gets query for [[BookGenres]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBookGenres()
    {
        return $this->hasMany(BookGenre::className(), ['book_id' => 'id']);
    }

    /**
     * Gets query for [[Genres]].
     *
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getGenres()
    {
        return $this->hasMany(Genre::className(), ['id' => 'genre_id'])->viaTable('book_genre', ['book_id' => 'id']);
    }

    // =======================
    // getters
    // =======================

    public function getFormattedPublicAt() : string
    {
        return DateHelper::asRusDate(strtotime($this->public_at));
    }

    /**
     * @return Author[]|array|null
     */
    public function getAllAuthors()
    {
        return $this->getAuthors()->all();
    }

    /**
     * @return Genre[]|array|null
     */
    public function getAllGenres()
    {
        return $this->getGenres()->all();
    }

    // =======================
    // validates
    // =======================

    public function validatePublicAt()
    {
        if (strtotime($this->public_at) >= time()) {
            $this->addError('public_at', 'Дата публикации еще не наступила!');
        }
    }
}
