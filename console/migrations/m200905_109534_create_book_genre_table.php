<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%book_genre}}`.
 */
class m200905_109534_create_book_genre_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%book_genre}}', [
            'id'       => $this->primaryKey(),
            'book_id'  => $this->integer(11)->notNull()->comment('ПК Книги'),
            'genre_id' => $this->integer(11)->notNull()->comment('ПК жанра')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%book_genre}}');
    }
}
