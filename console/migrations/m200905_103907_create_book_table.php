<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%book}}`.
 */
class m200905_103907_create_book_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%book}}', [
            'id'          => $this->primaryKey(),
            'name'        => $this->string(250)->notNull()->comment('Название книги'),
            'description' => $this->text()->notNull()->comment('Описание'),
            'image'       => $this->string(250)->comment('Обложка'),
            'public_at'   => $this->integer(11)->notNull()->comment('Дата публикации')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%book}}');
    }
}
