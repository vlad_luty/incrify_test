<?php

use yii\db\Migration;

/**
 * Add indexes & foreign keys
 */
class m200905_110731_bind_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex(
            '{{%idx-author-book-unique}}',
            '{{%author_book}}',
            ['author_id', 'book_id'],
            true
        );

        $this->addForeignKey(
            '{{%fk-author_id-author-book}}',
            '{{%author_book}}',
            'author_id',
            '{{%author}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            '{{%fk-book_id-author-book}}',
            '{{%author_book}}',
            'book_id',
            '{{%book}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            '{{%idx-book-genre-unique}}',
            '{{%book_genre}}',
            ['book_id', 'genre_id'],
            true
        );

        $this->addForeignKey(
            '{{%fk-book_id-book-genre}}',
            '{{%book_genre}}',
            'book_id',
            '{{%book}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            '{{%fk-genre_id-book-genre}}',
            '{{%book_genre}}',
            'genre_id',
            '{{%genre}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-author_id-author-book}}', '{{%author_book}}');
        $this->dropForeignKey('{{%fk-book_id-author-book}}', '{{%author_book}}');
        $this->dropIndex('{{%idx-author-book-unique}}', '{{%author_book}}');

        $this->dropForeignKey('{{%fk-book_id-book-genre}}', '{{%book_genre}}');
        $this->dropForeignKey('{{%fk-genre_id-book-genre}}', '{{%book_genre}}');
        $this->dropIndex('{{%idx-book-genre-unique}}', '{{%book_genre}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200905_104731_bind_tables cannot be reverted.\n";

        return false;
    }
    */
}
