<?php
namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;


class EndAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
    ];
    public $js = [
        'js/main.js'
    ];

    public function init() {
        $this->jsOptions['position'] = \yii\web\View::POS_END;
        parent::init();
    }

    public $depends = [
    ];
}