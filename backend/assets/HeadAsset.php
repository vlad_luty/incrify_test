<?php
namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;


class HeadAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/site.css'
    ];

    public function init() {
        $this->jsOptions['position'] = \yii\web\View::POS_HEAD;
        parent::init();
    }

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset',
    ];
}