'use strict';

$(document).on('click', '.add-author', (evnt) => {
   let html = $(".author-template").clone();
   html.removeClass('author-template');
   html.find('.form-control').attr('disabled', false);
   $('.authors-wrapper').append(html);
});


$(document).on('click', '.add-genre', (evnt) => {
    let html = $(".genre-template").clone();
    html.removeClass('genre-template');
    html.find('.form-control').attr('disabled', false);
    html.attr('disabled', false);
    $('.genres-wrapper').append(html);
});

$(".delete").on('click', (evnt) => {
    console.log('test');
    $(this).addClass('d-none');
});

$(document).on('click', '.delete', (evnt) => {
    evnt.target.parentNode.parentNode.remove();
});