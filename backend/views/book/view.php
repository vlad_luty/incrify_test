<?php

use common\models\Book;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Book */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="author-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function($model) {
                    /** @var $model Book */
                    return Html::img(Yii::$app->request->baseUrl . $model->image, [
                        'width' => '150'
                    ]);
                }
            ],
            [
                'attribute' => 'public_at',
                'format' => 'raw',
                'value' => function($model) {
                    /** @var $model Book */
                    return $model->getFormattedPublicAt();
                }
            ],
            [
                'attribute' => 'authors',
                'label' => 'Авторы',
                'format' => 'raw',
                'value' => function($model) {
                    /** @var $model Book */
                    return implode(', ', \yii\helpers\ArrayHelper::map($model->getAllAuthors(), 'id', function (\common\models\Author $author) {
                        return $author->getFullName();
                    }));
                }
            ],
            [
                'attribute' => 'genres',
                'label' => 'Жанры',
                'format' => 'raw',
                'value' => function($model) {
                    /** @var $model Book */
                    return implode(', ', \yii\helpers\ArrayHelper::map($model->getAllGenres(), 'id', 'name'));
                }
            ]
        ],
    ]) ?>

</div>
