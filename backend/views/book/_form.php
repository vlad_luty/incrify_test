<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Book */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <?= $form->field($model, 'public_at')->input('date') ?>

    <div class="authors-wrapper" style="margin-bottom: 10px;">
        <?php foreach ($model->getAllAuthors() as $i => $author) { ?>
            <?= Html::textInput("Book[authors][]", $author->getFullName(), ['disabled' => true, 'class' => 'form-control']) ?>
        <?php } ?>


        <button type="button" class="btn btn-primary add-author" style="margin-bottom: 10px;">+ Автор</button>

        <?php foreach (array_merge([0], $model->_authors) as  $author) { ?>

            <div class="form-group row <?= $author == 0? 'author-template' : '' ?>" style="margin: 0; margin-bottom: 10px;">
                <div class="col">
                    <?= Html::dropDownList("Book[authors][]", $author, \yii\helpers\ArrayHelper::merge([0 => '---'], \common\models\Author::getDropDown()), [
                        'disabled' => $author == 0,
                        'class' => 'form-control'
                    ]) ?>
                </div>
                <div class="col-auto">
                    <button type="button" class="btn btn-danger delete">Delete</button>
                </div>
            </div>
        <?php } ?>

    </div>

    <div class="genres-wrapper" style="margin-bottom: 10px;">
        <?php foreach ($model->getAllGenres() as $genre) { ?>
            <?= Html::textInput("Book[genres][]", $genre->name, ['disabled' => true]) ?>
        <?php } ?>

        <button type="button" class="btn btn-primary add-genre" style="margin-bottom: 10px;">+ Жанр</button>

        <?php foreach (array_merge([0], $model->_genres) as  $genre) { ?>
            <div class="form-group row <?= $genre == 0? 'genre-template' :'' ?>" style="margin: 0; margin-bottom: 10px;">
                <div class="col">

                    <?= Html::dropDownList("Book[genres][]", $genre, \yii\helpers\ArrayHelper::merge([0 => '---'], \common\models\Genre::getDropDown()), [
                        'disabled' => $genre == 0,
                        'class' => 'form-control'
                    ]) ?>
                </div>
                <div class="col-auto">
                    <button type="button" class="btn btn-danger delete">Delete</button>
                </div>
            </div>
        <?php } ?>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
